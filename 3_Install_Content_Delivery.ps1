#$ContentDeliverySourceDir = "C:\_install\SDL Web 8.5\Content Delivery\resources\quickinstall\"
#$INSTALLLOCATION_CD = "c:\Program Files (x86)\SDL Web\microservices"
#$CD_LICENSE_PATH = "C:\_install\SDL Web 8.5\licenses\cd_licenses.xml"
#$dbPort = 1433
#$ipDBserver = "localhost"

#check if it is possible to connect to DB over predefined port - default is 1433
$connection = New-Object System.Net.Sockets.TcpClient($ipDBserver, $dbPort)
if ($connection.Connected) {
    Write-Host "Connection to $ContentDeliveryDatabaseServer over $dbPort suceeded!" -ForegroundColor Green -BackgroundColor Yellow
    Write-Host "Starting installation of SDL Web 8.5 Content Delivery" -ForegroundColor Green -BackgroundColor Yellow

    cd $ContentDeliverySourceDir

    .\quickinstall.ps1 `
    -enable-discovery `
    -enable-deployer-combined `
    -enable-content `
    -enable-session `
    -enable-preview `
    -license $CD_LICENSE_PATH `
    -auto-register `
    -target-folder $INSTALLLOCATION_CD
}
else {
    Write-Host "Couldn't connect to $ContentDeliveryDatabaseServer over $dbPort - stopping the installation of SDL Web Content Delivery part" -ForegroundColor Red -BackgroundColor Yellow
}