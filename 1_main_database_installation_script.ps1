﻿<#
.SYNOPSIS
    Installing alldatabase for Content Manager part of SDL 8.5
.DESCRIPTION
    Installing alldatabase for Content Manager part of SDL 8.5
.NOTES
    Version : 2018-03-07
#>

#$installPath = "C:\_install\PS"
$installDBPath = "C:\_install\SDL Web 8.5\Database\mssql\"

#cd $installPath

#Import-Csv -Delimiter "," -LiteralPath ".\Variables_for_installation.csv" |

#ForEach-Object{

    #New-Variable -Name $_.variable -Value $_.value
     #   Write-Output "$($_.variable) = $($_.value) "
#}

#Write-Host "STARTING SDL DATA BASES INSTALLATION" -ForegroundColor Green

#region Content Manager

$ContentManagerDatabaseVars = @{}
$ContentManagerDatabaseVars.Add('DatabaseServer', $ContentManagerDatabaseServer)
$ContentManagerDatabaseVars.Add('DatabaseName', $ContentManagerDatabaseName) 
$ContentManagerDatabaseVars.Add('DatabaseUserName', $ContentManagerDatabaseUserName) 
$ContentManagerDatabaseVars.Add('DatabaseUserPassword', $ContentManagerDatabaseUserPassword) 
$ContentManagerDatabaseVars.Add('MTSUserName', $MTSUserName) 
$ContentManagerDatabaseVars.Add('WindowsSystemAccountName', $WindowsSystemAccountName)
$ContentManagerDatabaseVars.Add('DefaultTcmAdministratorUserName', $DefaultTcmAdministratorUserName) 
$ContentManagerDatabaseVars.Add('DefaultTcmAdministratorUserPassword', $DefaultTcmAdministratorUserPassword) 
$ContentManagerDatabaseVars.Add('AdministratorUserName', $AdministratorUserName) 
$ContentManagerDatabaseVars.Add('AdministratorUserPassword', $AdministratorUserPassword) 
$ContentManagerDatabaseVars.Add('NonInteractive', [boolean]$NonInteractive) 
$ContentManagerDatabaseVars.Add('IntegratedSecurity', [boolean]$IntegratedSecurity) 
$ContentManagerDatabaseVars.Add('ErrorAction', 'Stop')

$databaselist = Invoke-Sqlcmd -ServerInstance $ContentManagerDatabaseServer -Username $AdministratorUserName -Password $AdministratorUserPassword -Database Master `
    -Query "SELECT Name as DatabaseList, database_id, create_date FROM sys.databases
            WHERE Name NOT IN ('master', 'tempdb', 'model', 'msdb')"

cd $installDBPath

if ($databaselist.databaselist -notcontains $ContentManagerDatabaseName)
{
    Write-Output "INSTALLING CONTENT MANAGER DATABASE"

    & ".\Install Content Manager Database.ps1" @ContentManagerDatabaseVars

    Write-Output "CONTENT MANAGER DATABASE $ContentManagerDatabaseName HAS BEEN INSTALLED"
}
Else
{
    Write-Output "$ContentManagerDatabaseName exists on server $ContentManagerDatabaseServer. Will not create database"
}

#endregion Content Manager

#region Topology Manager Database 

$TopologyManagerDatabaseVars = @{}
$TopologyManagerDatabaseVars.Add('DatabaseServer', $TopologyManagerDatabaseServer)
$TopologyManagerDatabaseVars.Add('DatabaseName', $TopologyManagerDatabaseName)
$TopologyManagerDatabaseVars.Add('NonInteractive', [boolean]$NonInteractive)
$TopologyManagerDatabaseVars.Add('IntegratedSecurity', [boolean]$IntegratedSecurity)
$TopologyManagerDatabaseVars.Add('DatabaseUsers', @(@{UserName="$TopologyManagerDatabaseUserName";UserPassword="$TopologyManagerDatabaseUserPassword"}))
$TopologyManagerDatabaseVars.Add('ErrorAction', 'Stop')

$databaselist = Invoke-Sqlcmd -ServerInstance $TopologyManagerDatabaseServer -Username $AdministratorUserName -Password $AdministratorUserPassword -Database Master `
    -Query "SELECT Name as DatabaseList, database_id, create_date FROM sys.databases
            WHERE Name NOT IN ('master', 'tempdb', 'model', 'msdb')"

if ($databaselist.databaselist -notcontains $TopologyManagerDatabaseName)
{
    Write-Output "INSTALLING TOPOLOGY MANAGER DATABASE"

    & ".\Install Topology Manager database.ps1" @TopologyManagerDatabaseVars
    
    Write-Output "TOPOLOGY MANAGER DATABASE HAS BEEN INSTALLED"
}
Else
{
    Write-Output "$TopologyManagerDatabaseName exists on server $TopologyManagerDatabaseServer. Will not create database"
}

#endregion Topology Manager Database 

#region Content Delivery Database 

$ContentDeliveryDatabaseVars = @{}
$ContentDeliveryDatabaseVars.Add('DatabaseServer', $ContentDeliveryDatabaseServer)
$ContentDeliveryDatabaseVars.Add('DatabaseName', $ContentDeliveryDatabaseName)
$ContentDeliveryDatabaseVars.Add('DatabaseUserName', $ContentDeliveryDatabaseUserName)
$ContentDeliveryDatabaseVars.Add('DatabaseUserPassword', $ContentDeliveryDatabaseUserPassword)
$ContentDeliveryDatabaseVars.Add('NonInteractive', [boolean]$NonInteractive)
$ContentDeliveryDatabaseVars.Add('IntegratedSecurity', [boolean]$IntegratedSecurity)
$ContentDeliveryDatabaseVars.Add('ErrorAction', 'Stop')

$databaselist = Invoke-Sqlcmd -ServerInstance $ContentDeliveryDatabaseServer -Username $AdministratorUserName -Password $AdministratorUserPassword -Database Master `
    -Query "SELECT Name as DatabaseList, database_id, create_date FROM sys.databases
            WHERE Name NOT IN ('master', 'tempdb', 'model', 'msdb')"

if ($databaselist.databaselist -notcontains $ContentDeliveryDatabaseName)
{
    Write-Output "INSTALLING TOPOLOGY MANAGER DATABASE"

    & ".\Install Content Data Store.ps1" @ContentDeliveryDatabaseVars
    
    Write-Output "TOPOLOGY MANAGER DATABASE HAS BEEN INSTALLED"
}
Else
{
    Write-Output "$ContentDeliveryDatabaseName exists on server $ContentDeliveryDatabaseServer. Will not create database"
}

#endregion Content Delivery Database 

#region Content Broker Session Database 

$BrokerSessionDatabaseVars = @{}
$BrokerSessionDatabaseVars.Add('DatabaseServer', $BrokerSessionDatabaseServer)
$BrokerSessionDatabaseVars.Add('DatabaseName', $BrokerSessionDatabaseName)
$BrokerSessionDatabaseVars.Add('DatabaseUserName', $BrokerSessionDatabaseUserName)
$BrokerSessionDatabaseVars.Add('DatabaseUserPassword', $BrokerSessionDatabaseUserPassword)
$BrokerSessionDatabaseVars.Add('NonInteractive', [boolean]$NonInteractive)
$BrokerSessionDatabaseVars.Add('IntegratedSecurity', [boolean]$IntegratedSecurity)
$BrokerSessionDatabaseVars.Add('ErrorAction', 'Stop')

$databaselist = Invoke-Sqlcmd -ServerInstance $BrokerSessionDatabaseServer -Username $AdministratorUserName -Password $AdministratorUserPassword -Database Master `
    -Query "SELECT Name as DatabaseList, database_id, create_date FROM sys.databases
            WHERE Name NOT IN ('master', 'tempdb', 'model', 'msdb')"

if ($databaselist.databaselist -notcontains $BrokerSessionDatabaseName)
{
    Write-Output "INSTALLING TOPOLOGY MANAGER DATABASE"

    & ".\Install Content Data Store.ps1" @BrokerSessionDatabaseVars
    
    Write-Output "TOPOLOGY MANAGER DATABASE HAS BEEN INSTALLED"
}
Else
{
    Write-Output "$BrokerSessionDatabaseName exists on server $BrokerSessionDatabaseServer. Will not create database"
}

#endregion Broker Session Database 

#region Tridion Discovery Database 

$TridionDiscoveryDatabaseVars = @{}
$TridionDiscoveryDatabaseVars.Add('DatabaseServer', $TridionDiscoveryDatabaseServer)
$TridionDiscoveryDatabaseVars.Add('DatabaseName', $TridionDiscoveryDatabaseName)
$TridionDiscoveryDatabaseVars.Add('DatabaseUserName', $TridionDiscoveryDatabaseUserName)
$TridionDiscoveryDatabaseVars.Add('DatabaseUserPassword', $TridionDiscoveryDatabaseUserPassword)
$TridionDiscoveryDatabaseVars.Add('NonInteractive', [boolean]$NonInteractive)
$TridionDiscoveryDatabaseVars.Add('IntegratedSecurity', [boolean]$IntegratedSecurity)
$TridionDiscoveryDatabaseVars.Add('ErrorAction', 'Stop')

$databaselist = Invoke-Sqlcmd -ServerInstance $TridionDiscoveryDatabaseServer -Username $AdministratorUserName -Password $AdministratorUserPassword -Database Master `
    -Query "SELECT Name as DatabaseList, database_id, create_date FROM sys.databases
            WHERE Name NOT IN ('master', 'tempdb', 'model', 'msdb')"

if ($databaselist.databaselist -notcontains $TridionDiscoveryDatabaseName)
{
    Write-Output "INSTALLING TOPOLOGY MANAGER DATABASE"

    & ".\Install Content Data Store.ps1" @TridionDiscoveryDatabaseVars
    
    Write-Output "TOPOLOGY MANAGER DATABASE HAS BEEN INSTALLED"
}
Else
{
    Write-Output "$TridionDiscoveryDatabaseName exists on server $TridionDiscoveryDatabaseServer. Will not create database"
}

#endregion Tridion Discovery Database 