﻿$serviceslist = Get-Service -DisplayName "SDL Web Content Manager*"
$installParameters = @{}

$installParameters.Add('ACCEPT_EULA', [boolean]$ACCEPT_EULA)
$installParameters.Add('DB_NAME', $ContentManagerDatabaseName) 
$installParameters.Add('DB_PASSWORD', $ContentManagerDatabaseUserPassword) 
$installParameters.Add('DB_SERVER', $ContentManagerDatabaseServer) 
$installParameters.Add('DB_USER', $ContentManagerDatabaseUserName) 
$installParameters.Add('DB_TYPE', $DB_TYPE)
$installParameters.Add('TRIDION_CM_ENVIRONMENT_ID', $TRIDION_CM_ENVIRONMENT_ID) 
$installParameters.Add('SYSTEM_ACCOUNT_NAME', $SYSTEM_ACCOUNT_NAME) 
$installParameters.Add('SYSTEM_ACCOUNT_DOMAIN', $SYSTEM_ACCOUNT_DOMAIN) 
$installParameters.Add('SYSTEM_ACCOUNT_PASSWORD', $MTSUserPassword) 
$installParameters.Add('WEB_PORT', $WEB_PORT) 
$installParameters.Add('LICENSE_PATH', $LICENSE_PATH) 
$installParameters.Add('CD_LICENSE_PATH', $CD_LICENSE_PATH)
$installParameters.Add('TTM_DB_SERVER', $TTM_DB_SERVER)
$installParameters.Add('TTM_DB_NAME', $TTM_DB_NAME)
$installParameters.Add('TTM_DB_USER', $TTM_DB_USER)
$installParameters.Add('TTM_DB_PASSWORD', $TTM_DB_PASSWORD)
$installParameters.Add('TTM_WEB_PORT', $TTM_WEB_PORT)
$installParameters.Add('TTM_DB_TYPE', $TTM_DB_TYPE)
$installParameters.Add('INSTALLLOCATION', $INSTALLLOCATION)
$installParameters.Add('WEBLOCATION', $WEBLOCATION)


if ([string]::IsNullOrWhiteSpace($serviceslist) -eq $true)
{
    Write-Host "SDL Web 8.5 CM part will be installed. The server will be restarted automatically after the installation."
    
    $param = foreach ($pair in $installParameters.GetEnumerator()) {$pair.key + "=" + $pair.value }
    Write-Host $param

    cd $installCMPath

    .\SDLWeb85CM.exe -s $param -log $CMlogFile
 }   

Else
{
    Write-Host "SDL services exist, perhaps some SDL Web Content Manager parts were installed. Please check."
}